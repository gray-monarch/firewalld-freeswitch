## firewalld services and zones files for Freeswitch on CentOS 7.3 running on AWS

### Installation
```bash
git clone git@gitlab.com:gray-monarch/firewalld-freeswitch.git
cd firewalld-freeswitch/
cp services/* /etc/firewalld/services/
cp zones/* /etc/firewalld/zones/
systemctl restart firewalld 
```

### Verification
```bash
firewall-cmd --get-services
firewall-cmd --list-all-zones | less
iptables -nvL | less
```

### To allow an IP address permanent access to a zone
> *Note*: using the `--permanent` flag will add the IP to the zone, however, it will not be active until you reload firewalld

```bash
firewall-cmd --zone=fs_carriers --add-source="66.241.99.27" --permanent 
firewall-cmd --reload
firewall-cmd --list-all-zones
```